# Hesap Aksın Oyun

Pratik matematik hesaplamaları yapmanızı ve geliştirmenizi sağlayan eğlenceli, faydalı bir oyun.

Kendiniz ve belirleyeceğiniz rakiplerinizle kıyasıya mücadelenizi verip sonuç ve değişimleri gözlemleyin.

Hızlı ve bir o kadar da tam konsantre olmanız gerekmekte. Tamamen odaklanarak stratjinizi en iyi şekilde uygulayarak en büyük sayıya ulaşmaya çalışın.

# Oyun Nasıl Oynanır
"Oyunu Başlat" buttonuna basarak ekrana gelen sayılardan 30 saniye içerisinde hesabınızda ki en büyük sayıya ulaşmaya çalışın.
Kendinizle ve çevrenizdekiler ile yarışma fırsatı.

# Oyun Neler Kazandırır
Kısıtlı sürede maksimum değere ulaşabilmek için tam konsantre şekilde sayılara odaklanmalı. Sayılara sürekli üzerine katarak büyütülmesi gerektiği için dikkatli olmalı. Hafızayı böylece sürekli yoklayarak canlı tutmak gerekecek.
Pratik matemetik işlemleri yapmanızda büyük katkılar sağlar.
Hızlı olmanız gerekecek. Ama bu yeterli değil. İşlem sonucunuz da doğru olmalı.

# Lisans
Tahsin YÜKSEL
tahsinyuksel.com
info@tahsinyuksel.com

[tahsinyuksel.com](http://tahsinyuksel.com/)
[hesap aksın](http://tahsinyuksel.com/apps/hesapaksin/index.html)

