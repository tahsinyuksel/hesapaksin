var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

var Width = canvas.width;
var Height = canvas.height;

var flowSpeed = 1;

var gameLoop = null;

var endTime = null;

var gameObjects = [];

var processType = "+";

var totalCountNumber = 5;

var activeCountNumber = 0;

var minNumber = 0;

var maxNumber = 10;

var numberCreateFrequent = 0.02;

var MouseX, MouseY;

var isClicked = false;

var gameSettings = {
	"gameType": 1,
	"operation": "+",
	"numberResult": 0,
	"selectedNumberCount":0,
	"selectedItem": "",
	"gameTime": 10000
};

var numberObject = function(){
	this.x = 0;
	this.y = 0;
	this.number = Math.floor( (Math.random() * maxNumber) + minNumber);
	this.numberPixelSize = Math.floor( (Math.random() * 45) + 11);
	//this.numberPixelSize = 30;
	this.type = "numberObject";
	
	this.boxW = 0;
	this.boxH = 0;
	this.boxX = 0;
	this.boxY = 0;	
	
	var parent = this;
	
	this.draw = function(){
		ctx.font = this.numberPixelSize + "px Arial";
		ctx.fillText(this.number,this.x,this.y);

		//get text font width
		var metrics = ctx.measureText(this.number);		
		parent.boxW = metrics.width;	
		
		/* number in box. numara alanını belirtmek icin.
		// ayrica sayinin tiklama alanini gostermis oluyoruz.
		ctx.fillStyle="#FF0000";
		ctx.globalAlpha=0.2;
		ctx.fillRect(parent.x,parent.y - parent.numberPixelSize,parent.boxW,parent.numberPixelSize);
		*/
		
	}
}

var newCreateNumber = function(){
	if(Math.random() < numberCreateFrequent){
		var newNumber = new numberObject();
		newNumber.y = -20;
		newNumber.x = Math.floor(Math.random() * Width);
		gameObjects.push(newNumber);
	}
}


var playMoves = function(){
	ctx.clearRect(0,0,Width,Height);
	for(var i = 0; i < gameObjects.length; i++){
		if(gameObjects[i].y > Height){
			gameObjects.splice(i, 1);
		}else{
			gameObjects[i].y += flowSpeed;
			gameObjects[i].draw();
			//console.log(gameObjects[i].y);
			
			if(isClicked){
				
				if(hitTest(gameObjects[i].x, gameObjects[i].y - gameObjects[i].numberPixelSize, gameObjects[i].boxW, gameObjects[i].numberPixelSize, MouseX, MouseY)){
					//alert("collision");
					collisionOperation(gameObjects[i]);
					isClicked = false;
				}
				
				
			}
		}				
	}
}

	/* manuel test
	var newNumber = new numberObject();
		
	newX = Math.floor(Math.random() * Width);				
	
	newNumber.y = 50;
	newNumber.x = newX;
	newNumber.number = 5;
	
	gameObjects.push(newNumber);
	newNumber.draw();
	*/
		
		
/* html5 text işemleri kordinat sisteminde harf in alt noktasından başlıyor.
bu sebeple hittestlerde y kordinatları sorun oluşturdu. y noktası = y - h şeklinde standart klasik y değerine getirdik
*/
function hitTest(_objectX, _objectY, _boxW, _boxH, _mouseX, _mouseY){
	//console.log(_objectX +"-"+ _objectY +"-"+ _boxW +"-"+ _boxH +"-"+ _mouseX +"-"+ _mouseY);
	if( (_mouseX >= _objectX && _mouseX <= _objectX + _boxW) && (_mouseY >= _objectY && _mouseY <= _objectY + _boxH ) ){
		return true;
	}
	return false;
}
		
function play(){
	newCreateNumber();
	playMoves();
}

function collisionOperation(_selectedGameObject){
	if(gameSettings.gameType == 1){
		
		switch(gameSettings.operation){
			case "*":
				gameSettings.numberResult *= _selectedGameObject.number;
				break;
				
			default:
				gameSettings.numberResult += _selectedGameObject.number;
				break;
		}
		
	}
	gameSettings.selectedNumberCount += 1;
	gameSettings.selectedItem = _selectedGameObject.number;
	$('#selectedItemTxt').html(gameSettings.selectedItem);
}


canvas.addEventListener("click", canvasClick, false);

function canvasClick(event){
	MouseX = event.pageX - canvas.offsetLeft;
	MouseY = event.pageY - canvas.offsetTop;

	isClicked = true;
	
	/*
	click sirasinda control. test
	
	for(var i = 0; i < gameObjects.length; i++){
		console.log(gameObjects[i]);
		
		ctx.fillStyle="#FFFF00";
		ctx.globalAlpha=0.2;
		ctx.fillRect(gameObjects[i].x, gameObjects[i].y - gameObjects[i].numberPixelSize, gameObjects[i].boxW, gameObjects[i].numberPixelSize);
		
		if(hitTest(gameObjects[i].x, gameObjects[i].y - gameObjects[i].numberPixelSize, gameObjects[i].boxW, gameObjects[i].numberPixelSize, MouseX, MouseY)){
			alert("collision");
		}
	}
	*/
	
	//alert(MouseX + "-" + MouseY);
}


function gameEnd(){
	alert("game end");
	$('#totalNumberCountTxt').html(gameSettings.selectedNumberCount);
	$('#numberResultTxt').html(gameSettings.numberResult);
	
	clearInterval(gameLoop);
	clearTimeout(endTime);
}

$('#playGameBtn').click(function(){
	$('#operationTxt').html(gameSettings.operation);	
	
	gameLoop = setInterval(play,10);
	endTime = setTimeout(gameEnd,gameSettings.gameTime); // 5second
	
});


